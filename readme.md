# AGL Programming Challenge

# Minimum Requirements
Computer should have Windows 10 Anniversary Update (10.0; Build 14393) installed.


# Assumptions made

1. Animal types can only be cats, dogs, or fish (undefined otherwise)

# Running instructions

## Running the solution from Visual Studio 2017

1. Ensure that you have Visual Studio 2017 installed on a Windows Machine
2. Ensure that you have necessary UWP development components installed
3. Set AglProgrammingChallenge (Universal Windows) as the start up project
4. Run the solution


## Installing the solution via package in Windows 10 Developer mode

1. Ensure that you have windows developer mode enabled on your computer
2. Open the solution root in file explorer
3. Extract AglProgrammingChallenge_InstallPackage.zip
4. Open the extracted folder and right click Add-AppDevPackage.ps1
5. Select Run with PowerShell
6. Follow the on screen instructions
7. Open the AglProgrammingChallenge application from the Windows Start Menu

# Contact

For any further questions, please contact Mahmoud Abduo at mahmoud.abduo@kloud.com.au
