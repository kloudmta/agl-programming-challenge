﻿using AglProgrammingChallenge.BusinessLogic.Services.Interfaces;
using System;
using Windows.UI.Popups;

namespace AglProgrammingChallenge.Services
{
    public class DialogService : IDialogService
    {

        /// <summary>
        /// Shows a standard alert given a title and message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public async void ShowAlert(string title, string message)
        {
            // Create the message dialog and set its content
            var messageDialog = new MessageDialog(message, title);
            messageDialog.Commands.Add(new UICommand("OK"));

            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 0;

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 1;

            // Show the message dialog
            await messageDialog.ShowAsync();
        }
    }
}
