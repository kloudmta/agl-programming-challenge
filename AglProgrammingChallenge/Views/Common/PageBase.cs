﻿using Prism.Windows.Mvvm;

namespace AglProgrammingChallenge.Views.Common
{
    /// <summary>
    /// Page base to be used for all pages in the AglProgrammingChallenge project
    /// </summary>
    public abstract class PageBase : SessionStateAwarePage
    {
    }
}
