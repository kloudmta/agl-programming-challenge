﻿using AglProgrammingChallenge.BusinessLogic.Repositories;
using AglProgrammingChallenge.BusinessLogic.Repositories.Interfaces;
using AglProgrammingChallenge.BusinessLogic.Services;
using AglProgrammingChallenge.BusinessLogic.Services.Interfaces;
using AglProgrammingChallenge.Services;
using Prism.Mvvm;
using Prism.Windows;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;
using Windows.ApplicationModel.Activation;

namespace AglProgrammingChallenge
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : PrismApplication
    {
        // Unity dependency injection contianer to register and resolve types and instances
        private readonly IUnityContainer _container;


        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();

            _container = new UnityContainer();
        }


        protected override Task OnInitializeAsync(IActivatedEventArgs args)
        {
            _container.RegisterInstance(NavigationService);
            _container.RegisterInstance(SessionStateService);

            // Register Services here
            _container.RegisterType<IWebService, WebService>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IDialogService, DialogService>(new ContainerControlledLifetimeManager());

            // Register Repositories
            _container.RegisterType<IPeopleRepository, PeopleRepository>(new ContainerControlledLifetimeManager());

            SetupViewModelLocator();

            return base.OnInitializeAsync(args);
        }



        protected override Task OnLaunchApplicationAsync(LaunchActivatedEventArgs args)
        {
            // Entry point: go to main page
            NavigationService.Navigate("Main", null);
            return Task.CompletedTask;
        }


        /// <summary>
        /// Resolve unity container view model types when using constructor dependency injection
        /// </summary>
        protected override object Resolve(Type type)
        {
            return _container.Resolve(type);
        }


        private void SetupViewModelLocator()
        {
            // Set ViewModelLocator AutoWire path to AglProgrammingChallenge.BusinessLogic solution
            ViewModelLocationProvider.SetDefaultViewTypeToViewModelTypeResolver((viewType) =>
            {
                var viewModelTypeName = string.Format(CultureInfo.InvariantCulture, "AglProgrammingChallenge.BusinessLogic.ViewModels.{0}ViewModel, AglProgrammingChallenge.BusinessLogic", viewType.Name);
                var viewModelType = Type.GetType(viewModelTypeName);
                return viewModelType;
            });
        }

    }
}
