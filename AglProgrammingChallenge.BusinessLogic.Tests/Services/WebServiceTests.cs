﻿using AglProgrammingChallenge.BusinessLogic.Services;
using AglProgrammingChallenge.BusinessLogic.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace AglProgrammingChallenge.BusinessLogic.Tests.Services
{
    [TestClass]
    public class WebServiceTests
    {
        private const string AglUrl = "http://agl-developer-test.azurewebsites.net/people.json";
        private static IUnityContainer _container = new UnityContainer();

        [ClassInitialize]
        public static void SetupTests(TestContext context)
        {
            _container.RegisterType<IWebService, WebService>(new ContainerControlledLifetimeManager());
        }

        [TestMethod]
        public async Task GetMethodReturnsObjectTest()
        {
            var webService = _container.Resolve<IWebService>();

            dynamic result = await webService.Get<dynamic>(AglUrl);

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count > 0);
        }
    }
}
