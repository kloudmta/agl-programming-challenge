﻿using AglProgrammingChallenge.BusinessLogic.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;

namespace AglProgrammingChallenge.BusinessLogic.Tests.FakeServices
{
    public class FakeWebService : IWebService
    {
        public async Task<T> Get<T>(string url)
        {
            string resourceName = string.Empty;
            if (string.Equals(url, "http://agl-developer-test.azurewebsites.net/people.json"))
            {
                resourceName = "FakeData.json";
            }

            StorageFolder folder = Windows.ApplicationModel.Package.Current.InstalledLocation;
            folder = await folder.GetFolderAsync("Resources");
            StorageFile file = await folder.GetFileAsync(resourceName);

            using (var reader = new StreamReader(await file.OpenStreamForReadAsync()))
            {
                var content = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(content);
            }
        }
    }
}
