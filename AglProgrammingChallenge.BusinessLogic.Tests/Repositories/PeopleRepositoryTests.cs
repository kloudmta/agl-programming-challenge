﻿using AglProgrammingChallenge.BusinessLogic.Models;
using AglProgrammingChallenge.BusinessLogic.Repositories;
using AglProgrammingChallenge.BusinessLogic.Repositories.Interfaces;
using AglProgrammingChallenge.BusinessLogic.Services.Interfaces;
using AglProgrammingChallenge.BusinessLogic.Tests.FakeServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity;
using static AglProgrammingChallenge.BusinessLogic.Models.Animal;
using static AglProgrammingChallenge.BusinessLogic.Models.Person;

namespace AglProgrammingChallenge.BusinessLogic.Tests.Repositories
{
    [TestClass]
    public class PeopleRepositoryTests
    {
        private static IUnityContainer _container = new UnityContainer();
        private static IPeopleRepository _peopleRepository;

        [ClassInitialize]
        public static void SetupTests(TestContext context)
        {
            _container.RegisterType<IWebService, FakeWebService>();
            _container.RegisterType<IPeopleRepository, PeopleRepository>();

            _peopleRepository = _container.Resolve<IPeopleRepository>();
        }


        [TestMethod]
        public async Task GetPeopleReturnPeopleArrayTest()
        {
            var result = await _peopleRepository.GetPeopleAsync();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count() > 0);
        }


        [TestMethod]
        public async Task GetSortedCatsCorrectOrderTest()
        {
            var result = await _peopleRepository.GetSortedCatsAsync();

            var expectedMaleList = result[GenderTypes.Male].OrderBy(a => a.Name);
            var expectedFemaleList = result[GenderTypes.Female].OrderBy(a => a.Name);

            Assert.IsTrue(expectedMaleList.SequenceEqual(result[GenderTypes.Male]));
            Assert.IsTrue(expectedFemaleList.SequenceEqual(result[GenderTypes.Female]));
        }


        [TestMethod]
        public async Task GetSortedCatsCorrectNumberOfCatsTest()
        {
            var result = await _peopleRepository.GetSortedCatsAsync();

            Assert.AreEqual(result[GenderTypes.Female].Count(), 3);
            Assert.AreEqual(result[GenderTypes.Male].Count(), 4);
        }


        [TestMethod]
        public async Task GetSortedCatsOnlyCatsTest()
        {
            var result = await _peopleRepository.GetSortedCatsAsync();

            var allCats = new List<Animal>();
            allCats.AddRange(result[GenderTypes.Male]);
            allCats.AddRange(result[GenderTypes.Female]);

            foreach (var cat in allCats)
            {
                Assert.IsTrue(cat.Type == AnimalType.Cat);
            }
        }


        [TestMethod]
        public async Task GetSortedCatsCorrectOwnerGendersTest()
        {
            // Get all people
            var people = await _peopleRepository.GetPeopleAsync();

            // Get expected owner lists
            var expectedMaleOwnerCats = GetCatsBasedOnOwnersGender(people, GenderTypes.Male);
            var expectedFemaleOwnerCats = GetCatsBasedOnOwnersGender(people, GenderTypes.Female);

            // Get actual result
            var actualResult = await _peopleRepository.GetSortedCatsAsync();

            // Check male owner expected list matches actual list
            foreach (var cat in actualResult[GenderTypes.Male])
            {
                Assert.IsTrue(expectedMaleOwnerCats.Contains(cat));
                expectedMaleOwnerCats.Remove(cat);
            }

            // Check female owner expected list matches actual list
            foreach (var cat in actualResult[GenderTypes.Female])
            {
                Assert.IsTrue(expectedFemaleOwnerCats.Contains(cat));
                expectedFemaleOwnerCats.Remove(cat);
            }
        }

        private List<Animal> GetCatsBasedOnOwnersGender(IEnumerable<Person> people, GenderTypes gender)
        {
            return people.Where(p => p.Gender == gender)
                                      .Where(p => p.Pets != null)
                                      .SelectMany(p => p.Pets)
                                      .Where(a => a.Type == AnimalType.Cat)
                                      .ToList();
        }
    }
}
