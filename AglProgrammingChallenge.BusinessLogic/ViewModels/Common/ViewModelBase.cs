﻿using Prism.Windows.Mvvm;

namespace AglProgrammingChallenge.BusinessLogic.ViewModels.Common
{
    /// <summary>
    /// AglProgrammingChallenge View Model Base
    /// </summary>
    public abstract class AglViewModelBase : ViewModelBase
    {
    }
}
