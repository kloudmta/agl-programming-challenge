﻿using AglProgrammingChallenge.BusinessLogic.Models;
using AglProgrammingChallenge.BusinessLogic.Repositories.Interfaces;
using AglProgrammingChallenge.BusinessLogic.Services.Interfaces;
using AglProgrammingChallenge.BusinessLogic.ViewModels.Common;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using static AglProgrammingChallenge.BusinessLogic.Models.Person;

namespace AglProgrammingChallenge.BusinessLogic.ViewModels
{
    /// <summary>
    /// ViewModel for Main Page
    /// </summary>
    public class MainPageViewModel : AglViewModelBase
    {
        private readonly IPeopleRepository _peopleRepository;
        private readonly IDialogService _dialogService;

        private ObservableCollection<Animal> _femaleOwnerCats;
        private ObservableCollection<Animal> _maleOwnerCats;
        private bool _isLoading;

        public MainPageViewModel(IPeopleRepository peopleRepository,
            IDialogService dialogService)
        {
            _peopleRepository = peopleRepository;
            _dialogService = dialogService;

            _maleOwnerCats = new ObservableCollection<Animal>();
            _femaleOwnerCats = new ObservableCollection<Animal>();

            LoadCats();
        }

        public DelegateCommand RefreshCatsCommand => new DelegateCommand(RefreshCats);

        public ObservableCollection<Animal> FemaleOwnerCats
        {
            get { return _femaleOwnerCats; }
            private set { SetProperty(ref _femaleOwnerCats, value); }
        }


        public ObservableCollection<Animal> MaleOwnerCats
        {
            get { return _maleOwnerCats; }
            private set { SetProperty(ref _maleOwnerCats, value); }
        }


        /// <summary>
        /// Whether the MainPageViewModel is loading something or not
        /// </summary>
        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }



        private async void LoadCats()
        {
            IsLoading = true;

            try
            {
                var cats = await _peopleRepository.GetSortedCatsAsync();

                AddCats(cats[GenderTypes.Female], FemaleOwnerCats);
                AddCats(cats[GenderTypes.Male], MaleOwnerCats);
            }
            catch (WebException)
            {
                _dialogService.ShowAlert("An error occurred.",
                    "An error occurred while communicating with the server. Please check your internet connection.");
            }
            catch (Exception)
            {
                _dialogService.ShowAlert("An unknown error occurred.",
                    "Unable to load cats, please try again");
            }

            IsLoading = false;
        }

        private void AddCats(IEnumerable<Animal> source, ObservableCollection<Animal> target)
        {
            foreach (var cat in source)
            {
                target.Add(cat);
            }
        }


        private void RefreshCats()
        {
            FemaleOwnerCats.Clear();
            MaleOwnerCats.Clear();
            LoadCats();
        }

    }
}
