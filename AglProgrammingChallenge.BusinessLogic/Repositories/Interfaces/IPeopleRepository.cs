﻿using AglProgrammingChallenge.BusinessLogic.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static AglProgrammingChallenge.BusinessLogic.Models.Person;

namespace AglProgrammingChallenge.BusinessLogic.Repositories.Interfaces
{
    /// <summary>
    /// Repository to retrieve people and pet data
    /// </summary>
    public interface IPeopleRepository
    {
        /// <summary>
        /// Get the list of all people and their pets via the AGL API
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Person>> GetPeopleAsync();

        /// <summary>
        /// Gets a dictionary of cat lists grouped by male and female 
        /// GenderTypes keys, sorted alphabetically by cat name
        /// </summary>
        /// <returns></returns>
        Task<Dictionary<GenderTypes, IOrderedEnumerable<Animal>>> GetSortedCatsAsync();
    }
}
