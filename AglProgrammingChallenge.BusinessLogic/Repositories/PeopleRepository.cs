﻿using AglProgrammingChallenge.BusinessLogic.Models;
using AglProgrammingChallenge.BusinessLogic.Repositories.Interfaces;
using AglProgrammingChallenge.BusinessLogic.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static AglProgrammingChallenge.BusinessLogic.Models.Animal;
using static AglProgrammingChallenge.BusinessLogic.Models.Person;

namespace AglProgrammingChallenge.BusinessLogic.Repositories
{
    public class PeopleRepository : IPeopleRepository
    {
        private const string PeopleEndpoint = "http://agl-developer-test.azurewebsites.net/people.json";
        private readonly IWebService _webService;

        public PeopleRepository(IWebService webService)
        {
            _webService = webService;
        }

        /// <summary>
        /// Get the list of all people and their pets via the AGL API
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Person>> GetPeopleAsync()
        {
            var people = await _webService.Get<Person[]>(PeopleEndpoint);
            return people;
        }


        /// <summary>
        /// Gets thel list of all cats split into male and female 
        /// owner lists, sorted alphabetically by cat name
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<GenderTypes, IOrderedEnumerable<Animal>>> GetSortedCatsAsync()
        {
            var people = await GetPeopleAsync();

            // select all cats from people's pets lists
            // group into dictionary with Male/Female keys
            // and cat arrays under each key sorted alphabetically by name
            var sortedCatsDictionary = people.GroupBy(p => p.Gender)
                                             .ToDictionary(x => x.Key,
                                                           x => x.Where(p => p.Pets != null)
                                                                 .SelectMany(p => p.Pets)
                                                                 .Where(a => a.Type == AnimalType.Cat)
                                                                 .OrderBy(a => a.Name));

            return sortedCatsDictionary;
        }
    }
}
