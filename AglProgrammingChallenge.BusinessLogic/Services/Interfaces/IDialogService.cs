﻿namespace AglProgrammingChallenge.BusinessLogic.Services.Interfaces
{
    /// <summary>
    /// A service to be implemented by the UI project to show
    /// UI alerts to the user
    /// </summary>
    public interface IDialogService
    {
        /// <summary>
        /// Shows a standard alert given a title and message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        void ShowAlert(string title, string message);
    }
}
