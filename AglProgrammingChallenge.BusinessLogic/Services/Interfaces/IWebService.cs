﻿using System.Threading.Tasks;

namespace AglProgrammingChallenge.BusinessLogic.Services.Interfaces
{
    public interface IWebService
    {
        /// <summary>
        /// Downloads JSON data via a HTTP GET request and deserializes 
        /// data into a provided generic T type
        /// </summary>
        /// <typeparam name="T">Generic type to deserialize JSON into</typeparam>
        /// <param name="url">url used to perform HTTP GET request</param>
        /// <returns></returns>
        Task<T> Get<T>(string url);
    }
}
