﻿using AglProgrammingChallenge.BusinessLogic.Services.Interfaces;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace AglProgrammingChallenge.BusinessLogic.Services
{
    public class WebService : IWebService
    {
        /// <summary>
        /// Downloads JSON data via a HTTP GET request and deserializes 
        /// data into a provided generic T type
        /// </summary>
        /// <typeparam name="T">Generic type to deserialize JSON into</typeparam>
        /// <param name="url">url used to perform HTTP GET request</param>
        /// <returns></returns>
        public async Task<T> Get<T>(string url)
        {
            // Setup HTTP GET request
            HttpClient httpClient = new HttpClient();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = "GET";
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Accept = "application/json";

            // Perform GET request
            var httpResponse = await httpWebRequest.GetResponseAsync();

            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                // get content as string
                var content = streamReader.ReadToEnd();

                // Deserialize content
                var response = JsonConvert.DeserializeObject<T>(@content);

                return response;
            }
        }

    }
}
