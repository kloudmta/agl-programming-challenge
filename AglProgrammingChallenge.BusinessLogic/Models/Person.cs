﻿namespace AglProgrammingChallenge.BusinessLogic.Models
{

    public class Person
    {
        public enum GenderTypes
        {
            Female,
            Male,
        }

        public string Name { get; set; }
        public GenderTypes Gender { get; set; }
        public int Age { get; set; }
        public Animal[] Pets { get; set; }
    }
}
