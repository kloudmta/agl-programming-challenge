﻿namespace AglProgrammingChallenge.BusinessLogic.Models
{
    public class Animal
    {
        public enum AnimalType
        {
            Cat,
            Dog,
            Fish,
            Undefined
        }

        public string Name { get; set; }
        public AnimalType Type { get; set; }

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            var compareTo = obj as Animal;
            return string.Equals(this.Name, compareTo.Name) && this.Type == compareTo.Type;
        }

        public override int GetHashCode()
        {
            return (this.Name ?? string.Empty).GetHashCode() + this.Type.GetHashCode();
        }
    }
}
